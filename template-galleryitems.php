<?php
/*
Template Name: Gallery Items Template
*/
get_header(); ?>
<div id="main" class="container">
	<div class="row">
    <?php global $wp_query,$paged;
   $gallery = $post->post_name;
    $r = query_posts('post_type=galleryitem&post_status=publish&posts_per_page=-1&taxonomy=gallery&term=' . $gallery) ?>
    <section class="gallery-section twelvecol last">
            <?php if(have_posts()) : ?>
                    
                    <?php while(have_posts()) : the_post() ?>
                        <article class="gallery-post">
                                <div class="holder">
                       
                                        <a href="<?php the_permalink() ?>"> <?php the_post_thumbnail('351x9999') ?></a>
                                        <?php $t = wp_get_object_terms($post->ID, 'gallery');
                                        if(!empty($t)) {
                                            $taxonomy_link = '';
                                            foreach($t as $taxonomy){
	                                            $image = get_post(get_post_thumbnail_id($post->ID));
												$video_link = $image->post_content;
								            	//print_r($video_link);
								            	if ($video_link){
								            		$taxonomy_link .= '<a class="fancybox" data-fancybox-type="iframe" href="http://player.vimeo.com/video/' . $video_link .'?hd=1&autoplay=1&show_title=1&show_byline=1&show_portrait=0&color=&fullscreen=1">' . get_the_title() . '</a>';
							            		} /*
else {
	                                                $taxonomy_link .= '<a href="' . esc_attr(get_term_link($taxonomy, 'gallery')) . '">' . $taxonomy->name . '</a>';
							            		}
*/
                                                
                                            }    
                                        } ?>
                                        <header>
                                                <ul>
                                                    <?php if($taxonomy_link != '') : ?>
                                                        <li><?php echo $taxonomy_link; ?></li>
                                                    <?php else: ?>
                                                        <li><a href="<?php the_permalink() ?>"><?php the_title() ?></a></li>
                                                    <?php endif; ?>
                                                        <!-- <li><?php the_title() ?></li> -->
                                                </ul>
                                        </header>
                                    
    
                                </div>
                       </article>
            
                    <?php endwhile ?>
                
                </div>
            <?php endif ?>
    </section>
    <?php if(SHOW_NAV){ theme_nav(); } ?>
    <?php// theme_nav() ?>
    <?php wp_reset_query() ?>
    </div>
</div>
<?php get_footer(); ?>
<script type="text/JavaScript">
	function appendCallout() {
		setTimeout(function() {
			if(jQuery(".fancybox-wrap").length > 0) {
				jQuery(".fancybox-wrap").append("<div class='fullscreen-callout'><div class='arrow-up'></div>Go fullscreen for a better experience</div>");
				setTimeout(function() { jQuery('.fullscreen-callout').fadeOut(500) }, 2900);
			} else {
				appendCallout();
			}
		}, 250);
	}
	jQuery(document).ready(function() {
		jQuery("a.fancybox").on("click", function() {
			appendCallout();
		});
	});
</script>