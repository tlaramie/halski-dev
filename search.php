<?php get_header(); ?>
<div id="main">
	<section class="blog-section">
		<?php if(have_posts()) : ?>
			<div class="wp_title">
				<h1>Search Results</h1>
			</div>
			<?php while(have_posts()) : the_post(); ?>
				<article class="post">
					<?php get_template_part('sidebar', 'block') ?>
					<div class="descr">
						<?php $theme_post = get_post_meta(get_the_ID(), '_theme_post', true); ?>
						<?php if($theme_post == 'video') : ?>
							<?php get_template_part('post', 'video') ?>
						<?php elseif($theme_post == 'gallery') : ?>
							<?php get_template_part('post', 'gallery') ?>
						<?php else : ?>
							<div class="text-block">
								<?php theme_excerpt() ?>
							</div>
						<?php endif ?>
					</div>
				</article>
			<?php endwhile ?>
			<?php theme_nav() ?>
		<?php else : ?>
			<article class="post">
				<div class="descr">
					<div class="text-block">
						<h1>No posts found.</h1>
						<p> Try a different search?</p>
						<?php get_search_form(); ?>
					</div>
				</div>
			</article>
		<?php endif ?>
	</section>
</div>
<?php get_footer(); ?>