<?php get_header(); ?>
<div id="main" class="container">
	<section class="blog-section">
		<?php if(have_posts()) : ?>
			<?php while(have_posts()) : the_post(); ?>
			<?php $theme_post = get_post_meta(get_the_ID(), '_theme_post', true); ?>
				<article class="row post <?php echo $theme_post; ?>">
					<div class="onecol"></div>
					<?php get_template_part('sidebar', 'block') ?>
					<div class="descr sevencol">
						
						<?php if($theme_post == 'video') : //post with video?>
							<?php get_template_part('post', 'video') ?>
						<?php elseif($theme_post == 'gallery') : ?>
							<?php get_template_part('post', 'gallery')  //post with gallery images ?>
						<?php else :  //post with only text ?>
							<div class="text-block">
								<?php theme_excerpt() ?>
							</div>
						<?php endif ?>
					</div>
					<div class="onecol last"></div>
				</article>
			<?php endwhile ?>
			<?php theme_nav() ?>
		<?php else : ?>
			<article class="post">
				<div class="descr">
					<div class="text-block">
						<h1>Not Found</h1>
						<p>Sorry, but you are looking for something that isn't here.</p>
					</div>
				</div>
			</article>
		<?php endif ?>
	</section>
</div>
<?php get_footer(); ?>