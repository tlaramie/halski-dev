<?php

include( TEMPLATEPATH.'/constants.php' );
include( TEMPLATEPATH.'/classes.php' );
include( TEMPLATEPATH.'/widgets.php' );

function tg_scripts()
{
	//  wp_enqueue_script('modernizer', get_bloginfo('template_url') . '/js/modernizr-2.5.2-respond-1.1.0.min.js');     
	wp_enqueue_script('jquery');
	//wp_enqueue_script('masonry', get_bloginfo('template_url') . '/js/masonry.min.js', array('jquery'), false, true);
	wp_enqueue_script('masonry', get_bloginfo('template_url') . '/js/jquery.masonry.min.js', array('jquery'), false, true);
	if (is_page( 4 )) { //if homepage
		wp_enqueue_script('fittext', get_bloginfo('template_url') . '/js/jquery.fittext.js', array('jquery'), false, true);
		wp_enqueue_script('vegas', get_bloginfo('template_url') . '/js/jquery.vegas.js', array('jquery'), false, true); 
	}; 
	wp_enqueue_script('main', get_bloginfo('template_url') . '/js/jquery.main.js', array('jquery'), false, true);   
	wp_enqueue_script('fancybox', get_bloginfo('template_url') . '/js/jquery.fancybox.pack.js', array('jquery'), false, true);
	wp_enqueue_script('fancymedia', get_bloginfo('template_url') . '/js/jquery.fancybox-media.js', array('jquery'), false, true);  
	wp_enqueue_script('scripts', get_bloginfo('template_url') . '/js/scripts.js', array('jquery'), false, true); 
	 
}
add_action('wp_enqueue_scripts', 'tg_scripts');

/**
 * Disable automatic general feed link outputting.
 */
automatic_feed_links( false );

//remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_generator');

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'id' => 'form-sidebar',
		'name' => 'Form Sidebar',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => ''
	));
}

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 50, 50, true ); // Normal post thumbnails
	add_image_size( 'single-post-thumbnail', 400, 9999, true );
	add_image_size( '9999x9999', 9999, 9999, true );
	add_image_size( '423x273', 423, 273, true );
	add_image_size( '422x273', 422, 273, true );
	add_image_size( '647x347', 647, 347, true );
	add_image_size( '647x469', 647, 469, true );
	add_image_size( 'full-width-header', 1140, 273, true );
	
	
	// for single photogphy
	add_image_size( '746x569', 746, 569, true );
	add_image_size( '944x656', 944, 656, true );
	add_image_size( '451x666', 451, 666, true );
	add_image_size( '591x459', 591, 459, true );
	
	add_image_size( '547-width', 547, 9999 );
	add_image_size( '746-width', 746, 9999 );
	add_image_size( '944-width', 944, 9999 );
	add_image_size( '451-width', 451, 9999 );
	add_image_size( '591-width', 591, 9999 );
	
	// for single photogphy, other position
	add_image_size( '1139x9999', 1139, 9999, false );
	
	// for gallery
	add_image_size( '351x9999', 351, 9999, false );
	
	/* not need
	add_image_size( '351x282', 351, 282, true );
	add_image_size( '351x427', 351, 427, true );
	add_image_size( '351x276', 351, 276, true );
	add_image_size( '351x353', 351, 353, true );
	add_image_size( '351x281', 351, 281, true );
	add_image_size( '351x241', 351, 241, true );
	//add_image_size( '351x427', 351, 427, true );
	add_image_size( '351x284', 351, 284, true );
	add_image_size( '351x251', 351, 251, true );*/
}

register_nav_menus( array(
	'primary' => __( 'Primary Navigation', 'base' ),
	'about1' => __( 'About Navigation1', 'base' ),
	'about2' => __( 'About Navigation2', 'base' ),
	'bios' => __( 'Biography Navigation', 'base' ),
) );


//add [email]...[/email] shortcode
function shortcode_email($atts, $content) {
	$result = '';
	for ($i=0; $i<strlen($content); $i++) {
		$result .= '&#'.ord($content{$i}).';';
	}
	return $result;
}
add_shortcode('email', 'shortcode_email');

// register tag [template-url]
function filter_template_url($text) {
	return str_replace('[template-url]',get_bloginfo('template_url'), $text);
}
add_filter('the_content', 'filter_template_url');
add_filter('get_the_content', 'filter_template_url');
add_filter('widget_text', 'filter_template_url');

// register tag [site-url]
function filter_site_url($text) {
	return str_replace('[site-url]',get_bloginfo('url'), $text);
}
add_filter('the_content', 'filter_site_url');
add_filter('get_the_content', 'filter_site_url');
add_filter('widget_text', 'filter_site_url');


/* Replace Standart WP Menu Classes */
function change_menu_classes($css_classes) {
        $css_classes = str_replace("current-menu-item", "active", $css_classes);
        $css_classes = str_replace("current-menu-parent", "active", $css_classes);
        return $css_classes;
}
add_filter('nav_menu_css_class', 'change_menu_classes');


//allow tags in category description
$filters = array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description');
foreach ( $filters as $filter ) {
    remove_filter($filter, 'wp_filter_kses');
}


//Make WP Admin Menu HTML Valid
function wp_admin_bar_valid_search_menu( $wp_admin_bar ) {
	if ( is_admin() )
		return;

	$form  = '<form action="' . esc_url( home_url( '/' ) ) . '" method="get" id="adminbarsearch"><div>';
	$form .= '<input class="adminbar-input" name="s" id="adminbar-search" tabindex="10" type="text" value="" maxlength="150" />';
	$form .= '<input type="submit" class="adminbar-button" value="' . __('Search') . '"/>';
	$form .= '</div></form>';

	$wp_admin_bar->add_menu( array(
		'parent' => 'top-secondary',
		'id'     => 'search',
		'title'  => $form,
		'meta'   => array(
			'class'    => 'admin-bar-search',
			'tabindex' => -1,
		)
	) );
}
function fix_admin_menu_search() {
	remove_action( 'admin_bar_menu', 'wp_admin_bar_search_menu', 4 );
	add_action( 'admin_bar_menu', 'wp_admin_bar_valid_search_menu', 4 );
}
add_action( 'add_admin_bar_menus', 'fix_admin_menu_search' );


/* meta box */
add_action('admin_menu', 'theme_meta_box');
function theme_meta_box() {
	if ( function_exists('add_meta_box') ) {
		add_meta_box('sample_id', 'Post Options', 'theme_meta', 'post', 'side');
	}
}
function theme_meta() {
	global $post;

	wp_nonce_field( 'theme_sample', '_theme_sample_nonce', false, true );
	
	$theme_post = get_post_meta( $post->ID, '_theme_post', true);
	switch($theme_post){
		case 'video': $checked_video = ' checked="checked"'; break;
		case 'gallery': $checked_gallery = ' checked="checked"'; break;
		case 'article': $checked_article = ' checked="checked"'; break;
		default: $checked_article = ' checked="checked"';
	}
	?>
	<table>
		<tr>
			<td><input type="radio" id="post_article" name="theme_post" value="article"<?php echo $checked_article ?>></td>
			<td><label for="post_article">Article</label></td>
		</tr>
		<tr>
			<td><input type="radio" id="post_gallery" name="theme_post" value="gallery"<?php echo $checked_gallery ?>></td>
			<td><label for="post_gallery">Gallery</label></td>
		</tr>
		<tr>
			<td><input type="radio" id="post_video" name="theme_post" value="video"<?php echo $checked_video ?>></td>
			<td><label for="post_video">Video</label></td>
		</tr>
	</table>
	<?php
}
add_action('wp_insert_post', 'theme_insert_post');
function theme_insert_post($post_id) {
	if ( wp_verify_nonce( $_REQUEST['_theme_sample_nonce'], 'theme_sample' )) {

		if (isset( $_POST['theme_post']) &&  $_POST['theme_post'])
			update_post_meta($post_id, '_theme_post', $_POST['theme_post']);	
		else
			delete_post_meta($post_id, '_theme_post');
	}
}
function video($id, $width, $height){
	$video = get_post_meta($id, 'video', true);
	if($video){
		$video = str_replace('[template-url]', get_bloginfo('template_url'), $video);
		$video = preg_replace('/width="[\d]+"/', "width=\"{$width}\"", $video);
		return preg_replace('/height="[\d]+"/', "height=\"{$height}\"", $video);
	}
	return false;
}

function theme_nav(){
	$newer = get_next_posts_link( 'OLDER &raquo;', 0 );
	$newer = $newer ? $newer : 'OLDER &raquo;';
	
	$older = get_previous_posts_link( '&laquo; NEWER' );
	$older = $older ? $older : '&laquo; NEWER' ?>
	<nav class="blog-nav">
		<span><?php echo $older ?></span>
		<span><?php echo $newer ?></span>
	</nav>
	<?php 
}

function theme_excerpt(){
	$excerpt = get_the_excerpt() . ' <a href="' . get_permalink() . '" class="more">Read more</a>';
	$excerpt = apply_filters('the_content', $excerpt);
	$excerpt = str_replace(']]>', ']]&gt;', $excerpt);
	$excerpt = str_replace('[...]', '', $excerpt);
	echo $excerpt;
}

add_action( 'wp_print_styles', 'delete_styles_cf7', 100 );
 
function delete_styles_cf7() {
    wp_deregister_style( 'contact-form-7' );
}



add_action('init', 'codex_gallery_init');
function codex_gallery_init() 
{
  $labels = array(
    'name' => _x('Gallery Items', 'post type general name'),
    'singular_name' => _x('Gallery Item', 'post type singular name'),
    'add_new' => _x('Add New', 'galleryitem'),
    'add_new_item' => __('Add New Gallery Item'),
    'edit_item' => __('Edit Gallery Item'),
    'new_item' => __('New Gallery Item'),
    'all_items' => __('All Gallery Items'),
    'view_item' => __('View Gallery Item'),
    'search_items' => __('Search Gallery Items'),
    'not_found' =>  __('No gallery item found'),
    'not_found_in_trash' => __('No gallery items found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => 'Gallery Items'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields','trackbacks','comments','revisions','page-attributes')
  ); 
  register_post_type('galleryitem',$args);
}

add_filter('post_updated_messages', 'codex_gallery_updated_messages');
function codex_gallery_updated_messages( $messages ) {
  global $post, $post_ID;

  $messages['gallery item'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Gallery Item updated. <a href="%s">View gallery item</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Gallery Item updated.'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Gallery Item restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Gallery Item published. <a href="%s">View Gallery Item</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Gallery Item saved.'),
    8 => sprintf( __('Gallery Item submitted. <a target="_blank" href="%s">Preview gallery item</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Gallery Item scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview gallery item</a>'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Gallery Item draft updated. <a target="_blank" href="%s">Preview gallery item</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
}

add_action( 'init', 'create_gallery_item_taxonomies', 0 );

function create_gallery_item_taxonomies() 
{
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Gallery', 'taxonomy general name' ),
    'singular_name' => _x( 'Gallery', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Galleries' ),
    'all_items' => __( 'All Galleries' ),
    'parent_item' => __( 'Parent Gallery' ),
    'parent_item_colon' => __( 'Parent Gallery:' ),
    'edit_item' => __( 'Edit Gallery' ), 
    'update_item' => __( 'Update Gallery' ),
    'add_new_item' => __( 'Add New Gallery' ),
    'new_item_name' => __( 'New Gallery Name' ),
    'menu_name' => __( 'Gallery' ),
  ); 	

  register_taxonomy('gallery',array('galleryitem'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'gallery' ),
  ));
}

/*
add_action( 'admin_init', 'portfolio_meta_create' );
function portfolio_meta_create() {
    add_meta_box('portfolio_meta', 'Portfolio Info', 'portfolio_meta', 'portfolio', 'normal', 'high');
}

function portfolio_meta () {
    // - grab data 
    global $post;
    $custom = get_post_custom($post->ID);
    $portfolio_location = $custom["portfolio_location"][0];
    $portfolio_sqfeet = $custom["portfolio_sqfeet"][0];

    // - security -
    echo '<input type="hidden" name="portfolio-nonce" id="portfolio-nonce" value="' .
    wp_create_nonce( 'portfolio-nonce' ) . '" />';

    // - output -
    ?>
    <div class="tf-meta">
        <ul>
        	<li><label>Location</label><input name="portfolio_location" class="widefat" value="<?php echo $portfolio_location; ?>" /></li>
            <li><label>Square Feet</label><input name="portfolio_sqfeet" class="widefat" value="<?php echo $portfolio_sqfeet; ?>" /></li>
        </ul>
    </div>
    <?php
}

// Save Data
add_action ('save_post', 'save_portfolio_meta');
function save_portfolio_meta(){
    global $post;
    // - still require nonce
    if ( !wp_verify_nonce( $_POST['portfolio-nonce'], 'portfolio-nonce' )) {
        return $post->ID;
    }
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;

    // - save data
    if(!isset($_POST["portfolio_location"])):
        return $post;
        endif;
        $updateportfolio_location =  $_POST["portfolio_location"] ;
        update_post_meta($post->ID, "portfolio_location", $updateportfolio_location );
        
    if(!isset($_POST["portfolio_sqfeet"])):
        return $post;
        endif;
        $updateportfolio_sqfeet =  $_POST["portfolio_sqfeet"] ;
        update_post_meta($post->ID, "portfolio_sqfeet", $updateportfolio_sqfeet );
}
*/

function exclude_category( $query ) {
if ( $query->is_home() && $query->is_main_query() ) {
$query->set( 'cat', '-66' );
}
}
add_action( 'pre_get_posts', 'exclude_category' );


?>