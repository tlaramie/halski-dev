<div class="gallery">
        <?php $images=&get_children( 'post_parent='.get_the_ID().'&post_type=attachment&post_mime_type=image&orderby=menu_order&order=ASC' );
        if (is_array($images) && !empty($images)) : ?>
                <div class="frame">
                        <ul>
                                <?php foreach($images as $image) : ?>
                                        <li><?php echo wp_get_attachment_image( $image->ID, '647x469' ) ?></li>
                                <?php endforeach ?>
                        </ul>
                </div>
                <ul class="switcher">
                        <li><a href="#" class="prev">prev</a></li>
                        <li><a href="#" class="next">next</a></li>
                </ul>
        <?php endif ?>
</div>
<div class="text-block">
        <?php the_excerpt() ?>
</div>