
<?php $video = video(get_the_ID(), 647, 347) ?>
<?php if($video) : ?>
        <div class="video">
                <?php echo $video ?>
        </div>
<?php elseif ( has_post_thumbnail() ) : ?>
        <div class="video">
                <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('647x347'); ?></a>
        </div>
<?php endif; ?>
<div class="text-block">
        <?php the_excerpt() ?>
</div>