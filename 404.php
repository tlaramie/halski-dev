<?php get_header(); ?>
<div id="main">
	<section class="blog-section">
		<article class="post">
			<div class="descr">
				<div class="text-block">
					<h1>Not Found</h1>
					<p>Sorry, but you are looking for something that isn't here.</p>
				</div>
			</div>
		</article>
	</section>
</div>
<?php get_footer(); ?>