<?php

get_header(); ?>
<div id="main">
    <?php global $wp_query,$paged;
    $paged = $wp_query->query['paged'];
    $r = query_posts('post_type=galleryitem&posts_per_page=-1&paged=' . $paged) ?>
    <section class="gallery-section">
            <?php if(have_posts()) :
                $i = 1;
                $j = 1;
                $count = count($r);
                $class = array(' col01', '', ' col03'); ?>
                <div class="col col01">
                    
                    <?php while(have_posts()) : the_post() ?>
                        <article class="gallery-post">
                                <div class="holder">
                       
                                        <a href="<?php the_permalink() ?>"> <?php the_post_thumbnail('351x9999') ?></a>
                                        <?php $t = wp_get_object_terms($post->ID, 'gallery');
                                        if(!empty($t)) {
                                            $taxonomy_link = '';
                                            foreach($t as $taxonomy){
                                                $taxonomy_link .= '<a href="' . esc_attr(get_term_link($taxonomy, 'gallery')) . '">' . $taxonomy->name . '</a>';
                                            }    
                                        } ?>
                                        <header>
                                                <ul>
                                                    <?php if($taxonomy_link != '') : ?>
                                                       <!--  <li class="category"><?php echo $taxonomy_link ?></li> -->
                                                    <?php endif ?>
                                                        <li><a href="<?php the_permalink() ?>"><?php the_title() ?></a></li>
                                                        <!-- <li><?php the_title() ?></li> -->
                                                </ul>
                                        </header>
                                    
    
                                </div>
                                                        </article>
            <?php if($i % 3 == 0 && $i != $count) : ?>
                </div>
                <div class="col<?php echo $class[$j] ?>">
                        <?php if($j < 2){
                                $j++;
                            }else{
                                $j = 0;    
                            } ?>
            <?php endif ?>
                    <?php $i++; endwhile ?>
                
                </div>
            <?php endif ?>
    </section>
    <?php if(SHOW_NAV){ theme_nav(); } ?>
    <?php// theme_nav() ?>
    <?php wp_reset_query() ?>
</div>
<?php get_footer(); ?>