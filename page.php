<?php get_header(); ?>
<div id="main">
	<section class="blog-section">
		<?php if(have_posts()) : the_post() ?>
			<article class="post">
				<header>
					<h1><?php the_title() ?></h1>
					<em class="date"><?php the_time('F j, Y') ?></em>
				</header>
				<div class="descr">
					<?php $images=&get_children( 'post_parent='.get_the_ID().'&post_type=attachment&post_mime_type=image&orderby=menu_order&order=ASC' );
					if (is_array($images) && !empty($images)) : ?>
						<div class="gallery">	
							<div class="frame">
								<ul>
									<?php foreach($images as $image) : ?>
										<li><?php echo wp_get_attachment_image( $image->ID, '647x469' ) ?></li>
									<?php endforeach ?>
								</ul>
							</div>
							<ul class="switcher">
								<li><a href="#" class="prev">prev</a></li>
								<li><a href="#" class="next">next</a></li>
							</ul>
						</div>	
					<?php endif ?>
					<div class="text-block">
						<?php the_content() ?>
					</div>
					<?php $video = video(get_the_ID(), 647, 347) ?>
					<?php if($video) : ?>
						<div class="video">
							<?php echo $video ?>
						</div>
					<?php endif ?>
				</div>
			</article>
		<?php else : ?>
			<article class="post">
				<div class="descr">
					<div class="text-block">
						<h1>Not Found</h1>
						<p>Sorry, but you are looking for something that isn't here.</p>
					</div>
				</div>
			</article>
		<?php endif ?>
	</section>
</div>
<?php get_footer(); ?>