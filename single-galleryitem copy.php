<?php get_header() ?>
<div id="main">
    <?php if(have_posts()) : the_post() ?>    
        <?php $t = wp_get_object_terms($post->ID, 'gallery');
        $count = count($t);
        $taxonomy_name = '';
        $i = 1;
        foreach($t as $taxonomy) {
                if($i < $count)
                        $sep = ', ';
                else
                        $sep = '';
                
                $taxonomy_name .= $taxonomy->name . $sep;
                
                $i++;
        }
        
        $images=&get_children( 'post_parent='.get_the_ID().'&post_type=attachment&post_mime_type=image&orderby=menu_order&order=ASC' );

        if (is_array($images) && !empty($images)) {
            $i = 1;
            $gallery = '';
            $show_image = array();
            foreach($images as $image){
            	$imagetitle = get_the_title($image->ID);
            	$fullimage = wp_get_attachment_url($image->ID);
            	$video_link = $image->post_content;
            	if (!empty($video_link)){
            		$video_link = "http://player.vimeo.com/video/" . $video_link ."?hd=1&autoplay=1&show_title=1&show_byline=1&show_portrait=0&color=&fullscreen=1";
            		//echo($video_link);
	            	$fullimagelink = "<a data-fancybox-type='iframe' href='$video_link' class='fancybox' rel='gallery' alt='$imagetitle'>";
            	} else {
	            	$fullimagelink = "<a data-fancybox-type='image' href='$fullimage' class='fancybox' rel='gallery' alt='$imagetitle'>";
            	}
            	
            	//$fullimagelink = "<a data-fancybox-type='image' href='$fullimage' class='fancybox' rel='gallery01' title='$imagetitle'>";
                //$class = $i == 1 ? 'zoom' : 'hidden';
                //$gallery .= "<a href=\"{$image->guid}\" rel=\"gallery\" class=\"{$class}\">$imagetitle</a>\n";

                    switch($i){
                        case 1: $show_image[] = 
                        		$fullimagelink .  
                        		wp_get_attachment_image( $image->ID, '746-width', false, array('class' => 'alignleft') ) . 
                        		'</a>';
                        	break;
                        case 2: $show_image[] = 
                        		$fullimagelink .  
                        		wp_get_attachment_image( $image->ID, '944-width', false, array('class' => 'alignright')  ) . 
                        		'</a>'; 
                        	break;
                        case 3: $show_image[] = 
                        		$fullimagelink . 
                        		wp_get_attachment_image( $image->ID, '451-width', false, array('class' => 'alignleft')  ) . 
                        		'</a>';  
                        	break;
                        case 4: $show_image[] =
	                        		$fullimagelink .  
	                        		wp_get_attachment_image( $image->ID, '591-width') . 
	                        		'</a>';
                        		break;
                        default: $show_image[] = 
	                        		$fullimagelink .  
                        			wp_get_attachment_image( $image->ID, '1139x9999' ) . 
	                        		'</a>';
                    }
                    
                    
                $i++;
            }
        } ?>    
        <article class="project-article">
                <div class="holder">
                        <?php if(isset($show_image[0])) echo $show_image[0]; // show position 1 ?>
                        <div class="descr">
                                <header>
                                    <?php if($taxonomy_name != '') : ?>
                                        <h1><?php echo $taxonomy_name ?></h1>
                                    <?php endif ?>
                                        <strong class="author"><?php the_title() ?></strong>
                                </header>
                                <?php the_content() ?>
                                <?php //echo $gallery ?>
                                <a href="javascript:;" id="launcher" class="zoom"></a>
                        </div>
                </div>
                <?php if(isset($show_image[1])) echo $show_image[1]; // show position 2 ?>
                <div class="visual">
                        <?php if(isset($show_image[2])) echo $show_image[2]; // show position 3 ?>
                        <?php if(isset($show_image[3])) echo $show_image[3];  // show position 4 ?>
                        <?php $count = count($show_image);
                        if($count > 4){
                            for($j = 4; $j < $count; $j++){
                                echo $show_image[$j], "\n"; // show other position
                            }
                        } ?>
                </div>
        </article>
    <?php endif ?>
</div>
<?php get_footer() ?>