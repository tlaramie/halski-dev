<?php
/*
Template Name: About Template
*/
get_header(); ?>
<div id="main">
    <?php if(have_posts()) : the_post() ?>
	<?php $images = &get_children( 'post_parent='.$post->ID.'&post_type=attachment&post_mime_type=image&orderby=menu_order&order=ASC' );
	if (is_array($images) && !empty($images)) : ?>
            <section class="grey-bar">
                <div class="row">
                    <?php 
                            $page_id = 2973;
                            $page_data = get_page( $page_id ); 
                            echo apply_filters('the_content', $page_data->post_content);
                        ?>
                </div>
            </section>
        <?php endif ?>
            <div class="row container">
                <section id="content">
                        <!--<h1><?php the_title() ?></h1>-->
                        <?php the_content() ?>
                </section>
            </div>
            <div class="grey-bar" id="bio-pictures">
                    <div class="row container">
                        <div class="col-md-6 col-sm-6 bio-pic">
                            <?php 
                            if (has_post_thumbnail($page_id = 2452)){
                            echo get_the_post_thumbnail(
                            $page_id, "full", 
                            array('class' => 'post_thumbnail')
                            );
                            } 
                            ?>
                        </div>
                        <div class="col-md-6 col-sm-6 bio-pic">
                           <?php 
                            if (has_post_thumbnail($page_id = 306)){
                            echo get_the_post_thumbnail(
                            $page_id, "full", 
                            array('class' => 'post_thumbnail')
                            );
                            } 
                            ?>
                        </div>
                    </div>
            </div>
                <section>
                    <div class="row container">
                        <div class="col-md-6">
                            <div class="mobile-bio-pic">
                                 <?php 
                                if (has_post_thumbnail($page_id = 2452)){
                                echo get_the_post_thumbnail(
                                $page_id, "full", 
                                array('class' => 'post_thumbnail')
                                );
                                } 
                                ?>
                            </div>
                            <?php 
                                $page_id = 2452;
                                $page_data = get_page( $page_id ); 
                                echo apply_filters('the_content', $page_data->post_content);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <div class="mobile-bio-pic">
                                 <?php 
                                if (has_post_thumbnail($page_id = 306)){
                                echo get_the_post_thumbnail(
                                $page_id, "full", 
                                array('class' => 'post_thumbnail')
                                );
                                } 
                                ?>
                            </div>
                            <?php 
                                $page_id = 306;
                                $page_data = get_page( $page_id ); 
                                echo apply_filters('the_content', $page_data->post_content);
                            ?>
                        </div>
                    </div>
                </section>
                <section class="grey-bar">
                    <div class="row container">
                    <?php 
                            $page_id = 2986;
                            $page_data = get_page( $page_id ); 
                            echo apply_filters('the_content', $page_data->post_content);
                        ?>
                    </div>
                </section>
                <div class="row container">
                    <section>
                            <?php 
                                $page_id = 551;
                                $page_data = get_page( $page_id ); 
                                echo apply_filters('the_content', $page_data->post_content);
                            ?>
                    </section>
                </div>
                <section id="social-section">
                    <?php 
                            $page_id = 2997;
                            $page_data = get_page( $page_id ); 
                            echo apply_filters('the_content', $page_data->post_content);
                        ?>
                </section>
                    <section id="testimonials" >
                        <div class="row container">
                            <div class="col-md-4">&nbsp;</div>
                            <div class="col-md-8"><h2> <?php echo get_the_title(2999); ?></h2></div> 
                            <div class="col-md-8">
                                <?php $catquery = new WP_Query( 'category_name=testimonial&posts_per_page=23' ); ?>
                                <ul>
                                <?php while($catquery->have_posts()) : $catquery->the_post(); ?>
                                <li>
                                <ul><li><?php the_content(); ?></li>
                                </ul>
                                </li>
                                <?php endwhile; ?> 
                                </ul>
                                <?php wp_reset_postdata(); ?>
                            </div>
                            <div class="col-md-4">
                                <?php 
                                $page_id = 2999;
                                $page_data = get_page( $page_id ); 
                                echo apply_filters('the_content', $page_data->post_content);
                            ?>
                            </div>
                        </div>
                    </section>
        </div>
    <?php endif ?>
</div>
<?php get_footer(); ?>