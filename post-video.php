<?php if ( has_post_thumbnail() ) : ?>
        <div class="video">
                <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('647x347'); ?></a>
        </div>
<?php endif; ?>
<div class="text-block">
        <?php the_excerpt() ?>
</div>