jQuery(function($){
    
    $( "#slogan-txt" ).each(function() { $(this).fitText(); });
    $('.fancybox').fancybox({
	    titlePosition: "inside"
    });
    $("#launcher").on("click", function(){
    	$(".fancybox").eq(0).trigger("click");
	});
 
    var $gallerycontainer = $('.gallery-section');
    $gallerycontainer.imagesLoaded(function(){
	   $gallerycontainer.masonry({
    		itemSelector : '.gallery-post'
    	});
    });
    
    var $container = $('.visual');
    $container.imagesLoaded(function(){
	   $container.masonry({
    		itemSelector : '.gallery-item'
    	});
    });
    
    $('.btn-navbar').click(function(){
		$(".nav-collapse").slideToggle();
	});
    
     // fix sub nav on scroll
	var $win = $(window), 
		$nav = $('#header'), 
		navTop = $('#header').length && $('#header').offset().top + 100/*  - 40 */, 
		isFixed = 0,
		$winWidth = $win.width();
		
		/*
console.log($('#header').offset().top);
		console.log("navtop " + navTop);
*/
		
	processScroll()
	// hack sad times - holdover until rewrite for 2.1
	$nav.on('click', function () {
		if (!isFixed) setTimeout(function () { $win.scrollTop($win.scrollTop() - 47) }, 10)
	})
	$win.on('scroll', processScroll)
	function processScroll() {
		var i, scrollTop = $win.scrollTop();
		/* console.log("scrollTop " + scrollTop); */
		if (scrollTop >= navTop && !isFixed) {
			isFixed = 1
			$nav.addClass('navbar-fixed-top')
		} else if (scrollTop <= navTop && isFixed) {
			isFixed = 0
			$nav.removeClass('navbar-fixed-top')
		}
		
	};

});