<?php
/*
Template Name: Default Template
*/
get_header(); ?>
<div id="main" class="container">
    <section class="contact-section row">
        <?php if(have_posts()) : the_post() ?>
            <div class="descr fivecol">
                    <h1><?php the_title() ?></h1>
                    <?php the_content() ?>
            </div>
            <div class="sevencol last"><?php dynamic_sidebar('form-sidebar'); ?></div>
        <?php endif ?>
    </section>
</div>
<?php get_footer(); ?>