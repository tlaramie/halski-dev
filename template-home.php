<?php
/*
Template Name: Home Template
*/
get_header(); ?>
<div class="home-gallery">
	<?php 
	$images = &get_children( 'post_parent='.get_the_ID().'&post_type=attachment&post_mime_type=image&orderby=menu_order&order=ASC' );
	if (is_array($images) && !empty($images)) : ?>
		<script type="text/javascript">
			jQuery(function($){
			
				$.vegas('slideshow', {
					backgrounds:[
					<?php foreach($images as $image) : ?>
						<?php 
						$background_image = wp_get_attachment_image_src( $image->ID, 'full');
						$background_image_url = $background_image[0]; ?>
						{ src:'<?php echo $background_image_url ?>', fade:1000 },
					<?php endforeach ?>
						]
				});
			
			});
		</script>
		<?php if(have_posts()) : the_post() ?>
			<div class="slogan">
				<?php remove_filter('the_content','wpautop'); ?>
				<?php the_content(); ?>
				<?php add_filter('the_content','wpautop'); ?>
			</div>
		<?php endif ?>
		<ul class="switcher">
			<?php echo $switcher ?>
		</ul>
	<?php endif ?>
</div>
<?php get_footer(); ?>