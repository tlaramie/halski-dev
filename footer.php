	<?php if(!is_front_page()) : ?>
                <footer id="footer">
                        <div class="holder">
                                <p>Copyright <a href="#">Halski Studio</a> <?php echo date('Y') ?>, All Rights Reserved</p>
                        </div>
                </footer>
        <?php endif ?>
        </div>
        <?php wp_footer(); ?>
</body>
</html>