<header class="threecol">
	<?php if(is_single()) : ?>
		<h1><?php the_title() ?></h1>
	<?php else : ?>
		<h1><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h1>
	<?php endif ?>
	<em class="date"><?php the_time('F j, Y') ?></em>
	<h2>FILED UNDER:</h2>
	<?php the_category() ?>
	<h2>SHARE:</h2>
	<ul class="sharing">
		<li><a class="facebook" href="http://www.facebook.com/share.php?u=<?php echo urlencode( the_permalink() ) ?>&amp;t=<?php echo str_replace('+', '%20', urlencode( the_title() ) ) ?>">Facebook</a></li>
		<li><a class="twitter" href="http://twitter.com/home?status=<?php echo str_replace('+', '%20', urlencode( the_title() ) ) ?>%20-%20<?php echo urlencode( the_permalink() ) ?>">Twitter</a></li>
		<li><a class="pinterest" href="http://pinterest.com/">Pinterest</a></li>
	</ul>
	<h2>DISCUSS:</h2>
	<p><?php comments_number( 'no comments', '1 comment', '% comments' ); ?>. <a href="<?php comments_link() ?>" class="more">Add yours</a></p>
</header>