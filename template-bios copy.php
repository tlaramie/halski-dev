<?php

get_header(); ?>
<div id="main">
    <?php if(have_posts()) : the_post() ?>
        <div class="twocolumns clearfix">
                <section id="content" class="bio">
                		<?php the_post_thumbnail(array(423,273), array('class' => 'profile')); ?>
                        <h1><?php the_title() ?></h1>
                        <article><?php the_content() ?></article>
                </section>
    <?php endif ?>
                <aside class="aside other-bios">
<?php
	$currentpage = $post->ID;
    $page = $post->post_parent;
	$mypages = get_pages( array( 'child_of' => $page, 'sort_column' => 'post_date', 'sort_order' => 'asc', 'exclude' => $currentpage ) );
	$i = 20;
	$o = 40;

	foreach( $mypages as $page ) {		
		$content = $page->post_content;
		$title = $page->post_title;
		//$thumbnail = the_post_thumbnail();
		if ( ! $content ) // Check for empty page
			continue;

		$content = apply_filters( 'the_content', $content );
	?>
		
		<section class="content bio other-bios" style="left:<?php echo $i; ?>px; opacity:.<?php echo $o; ?>;">
        		<?php// the_post_thumbnail(array(423,273), array('class' => 'profile')); ?>
        		<a href="<?php echo get_page_link( $page->ID ); ?>"><?php echo get_the_post_thumbnail($page->ID, array(423,273), array('class' => 'profile')); ?></a>
                <h1><?php echo $title; ?></h1>
                <article><?php echo $content; ?></article>
        </section>

	<?php
	$i += 463;
	$o -= 20;
	}	
?>

                </aside>
        </div>
</div>
<?php get_footer(); ?>