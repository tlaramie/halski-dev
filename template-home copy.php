<?php
/*
Template Name: 
*/
get_header(); ?>
<div class="home-gallery">
	<?php 
	$images = &get_children( 'post_parent='.get_the_ID().'&post_type=attachment&post_mime_type=image&orderby=menu_order&order=ASC' );
	if (is_array($images) && !empty($images)) :
		$switcher = '' ?>
		<div class="frame">
			<ul>
				<?php $i = 1; foreach($images as $image) : ?>
					<?php $active = $i == 1 ? ' class="active"' : '' ?>
					<li<?php echo $active ?>>
						<div class="block">
							<div class="box">
								<?php echo wp_get_attachment_image( $image->ID, '9999x9999') ?>
							</div>
						</div>
					</li>
					<?php $switcher .= "<li{$active}><a href=\"#\">{$i}</a></li>\n" ?>
				<?php $i++; endforeach ?>
			</ul>
		</div>
		<?php if(have_posts()) : the_post() ?>
			<div class="slogan">
				<?php remove_filter('the_content','wpautop'); ?>
				<?php the_content(); ?>
				<?php add_filter('the_content','wpautop'); ?>
			</div>
		<?php endif ?>
		<ul class="switcher">
			<?php echo $switcher ?>
		</ul>
	<?php endif ?>
</div>
<?php get_footer(); ?>