<?php get_header(); ?>
<div id="main">
	<section class="blog-section">
		<?php if(have_posts()) : ?>
			<div class="wp_title">
				<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
				<?php /* If this is a category archive */ if (is_category()) { ?>
				<h1>Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h1>
				<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
				<h1>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1>
				<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
				<h1>Archive for <?php the_time('F jS, Y'); ?></h1>
				<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
				<h1>Archive for <?php the_time('F, Y'); ?></h1>
				<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
				<h1>Archive for <?php the_time('Y'); ?></h1>
				<?php /* If this is an author archive */ } elseif (is_author()) { ?>
				<h1>Author Archive</h1>
				<?php /* If this is a paged archive */ } else{ ?>
				<h1><?php single_cat_title(); ?></h1>
				<?php } ?>
			</div>
			<?php while(have_posts()) : the_post(); ?>
				<article class="post">
					<?php get_template_part('sidebar', 'block') ?>
					<div class="descr">
						<?php $theme_post = get_post_meta(get_the_ID(), '_theme_post', true); ?>
						<?php if($theme_post == 'video') : ?>
							<?php get_template_part('post', 'video') ?>
						<?php elseif($theme_post == 'gallery') : ?>
							<?php get_template_part('post', 'gallery') ?>
						<?php else : ?>
							<div class="text-block">
								<?php theme_excerpt() ?>
							</div>
						<?php endif ?>
					</div>
				</article>
			<?php endwhile ?>
			<?php theme_nav() ?>
		<?php else : ?>
			<article class="post">
				<div class="descr">
					<div class="text-block">
						<h1>Not Found</h1>
						<p>Sorry, but you are looking for something that isn't here.</p>
					</div>
				</div>
			</article>
		<?php endif ?>
	</section>
</div>
<?php get_footer(); ?>