<?php
/*
Template Name: Biography Template
*/
get_header(); ?>
<div id="main" class="container bios">
    <?php if(have_posts()) : the_post() ?>
                <section id="content" class="bio row">
                		<div class="fivecol"><?php the_post_thumbnail(array(423,273), array('class' => 'profile')); ?></div>
                        <article class="fivecol">
                        	<h1><?php the_title() ?></h1>
                        	<?php the_content() ?>
                        </article>
                        <div class="twocol last"></div>
                </section>
                <nav class="blog-nav row">
					<span><a href="/about/">&laquo; About Us</a></span>
                <?php
					$currentpage = $post->ID;
				    $page = $post->post_parent;
					$mypages = get_pages( array( 'child_of' => $page, 'sort_column' => 'post_date', 'sort_order' => 'asc', 'exclude' => $currentpage ) );
					foreach( $mypages as $page ) :		
						$title = $page->post_title;
				?>
					<span><a href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $title ?> &raquo;</a></span>
					<?php endforeach; ?>
				
				</nav>
    <?php endif ?>
</div>
<?php get_footer(); ?>