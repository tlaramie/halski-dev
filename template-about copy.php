<?php

get_header(); ?>
<div id="main" class="container">
    <?php if(have_posts()) : the_post() ?>
	<?php $images = &get_children( 'post_parent='.$post->ID.'&post_type=attachment&post_mime_type=image&orderby=menu_order&order=ASC' );
	if (is_array($images) && !empty($images)) : ?>
            <div class="visual-list row">
                    <ul>
                        <?php $i = 1; foreach($images as $image) : ?>
                            <?php $size = $i == 1 ? '423x273' : '422x273' ?>
                                <li><?php echo wp_get_attachment_image( $image->ID, 'full-width-header' ) ?></li>
                            <?php if($i == 2) break ?>
                        <?php $i++; endforeach ?>
                    </ul>
            </div>
        <?php endif ?>
        <div class="row">
                <section id="content" class="fivecol">
                        <h1><?php the_title() ?></h1>
                        <?php the_content() ?>
                </section>
                <aside class="aside">
                            <nav class="col01">
                                    <h2><a href="#">Select CLIENT EXPERIENCE</a></h2>
                                    <?php wp_nav_menu( array(
                                            'container'         => false,
                                            'theme_location' 	=> 'about1',
                                            'depth' 		=> 1,
                                            'items_wrap' 	=> '<ul>%3$s</ul>' ) ); ?>
                            </nav>
                        <nav class="col02">
                                <h2><a href="#">Bios</a></h2>
                                <?php wp_nav_menu( array(
                                            'container'         => false,
                                            'theme_location' 	=> 'bios',
                                            'depth' 		=> 1,
                                            'items_wrap' 	=> '<ul>%3$s</ul>' ) ); ?>
                        </nav>
                </aside>
        </div>
    <?php endif ?>
</div>
<?php get_footer(); ?>