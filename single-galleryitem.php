<?php get_header() ?>
<div id="main" class="gallery-items">
    <?php if(have_posts()) : the_post() ?>    
        <?php $t = wp_get_object_terms($post->ID, 'gallery');
        $count = count($t);
        $taxonomy_name = '';
        $i = 1;
        foreach($t as $taxonomy) {
                if($i < $count)
                        $sep = ', ';
                else
                        $sep = '';
                
                $taxonomy_name .= $taxonomy->name . $sep;
                
                $i++;
        }
        
        $images=&get_children( 'post_parent='.get_the_ID().'&post_type=attachment&post_mime_type=image&orderby=menu_order&order=ASC' );

        if (is_array($images) && !empty($images)) {
            $i = 1;
            $gallery = '';
            $show_image = array();
            foreach($images as $image){
            	$imagetitle = get_the_title($image->ID);
            	$fullimage = wp_get_attachment_url($image->ID);
            	$video_link = $image->post_content;
            	if (!empty($video_link)){
            		$fullvideo_link = "http://player.vimeo.com/video/" . $video_link ."?hd=1&autoplay=1&show_title=1&show_byline=1&show_portrait=0&color=&fullscreen=1";
            		//echo($video_link);
	            	$fullimagelink = "<a data-fancybox-type='iframe' href='" . $fullvideo_link . "' class='fancybox' rel='gallery' alt='$imagetitle'>";
            	} else {
	            	$fullimagelink = "<a data-fancybox-type='image' href='$fullimage' class='fancybox' rel='gallery' alt='$imagetitle'>";
            	}
            	
                    switch($i){
                        case 1: $show_image[] = 
                        		$fullimagelink .  
                        		wp_get_attachment_image( $image->ID, '547-width', false, array('class' => 'alignleft') ) . 
                        		'</a>';
                        	break;
                        default: $show_image[] = 
                        			'<div class="gallery-item">' .
	                        		$fullimagelink .  
                        			wp_get_attachment_image( $image->ID, '351x9999', 0 ) . 
	                        		'</a></div>';
                    }
                    
                    
                $i++;
            }
        } ?>    
        <article class="project-article">
                                
        <div class="container intro<?php if(!isset($show_image[1])) echo(" one-item");  ?>">
        	<div class="row">
                        <div class="sixcol"><?php if(isset($show_image[0])) echo $show_image[0]; // show position 1 ?></div>
                        <div class="descr fivecol">
                                <header>
                                    <?php if($taxonomy_name != '') : ?>
                                        <h1><?php echo $taxonomy_name ?></h1>
                                    <?php endif ?>
                                        <strong class="author"><?php the_title() ?></strong>
                                </header>
                                <?php the_content() ?>
                                <?php //echo $gallery ?>
                                <a href="javascript:;" id="launcher" class="zoom"></a>
                                <iframe style="display: none;" id="video_iframe" width="600" height="400" class="video-<?php echo $video_link ?>" src="//player.vimeo.com/video/<?php echo $video_link ?>?hd=1&show_title=1&show_byline=1&show_portrait=0&color=&fullscreen=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>
                        <div class="onecol last"></div>
        	</div>
        </div>
         <div class="container">
        	<div class="row">
                <div class="visual-wrapper twelvecol last">
                	 <?php if(isset($show_image[1])) echo("<h2>Additional " . get_the_title() . " Work</h2>");  ?>
	                <div class="visual">
	            		    <?php if(isset($show_image[1])) echo $show_image[1]; // show position 2 ?>
	                        <?php if(isset($show_image[2])) echo $show_image[2]; // show position 3 ?>
	                        <?php if(isset($show_image[3])) echo $show_image[3];  // show position 4 ?>
	                        <?php $count = count($show_image);
	                        if($count > 4){
	                            for($j = 4; $j < $count; $j++){
	                                echo $show_image[$j], "\n"; // show other position
	                            }
	                        } ?>
	                </div>
                </div>
        	</div>
         </div>
        </article>
    <?php endif ?>
</div>
<?php get_footer() ?>
<script type="text/JavaScript">
	function appendCallout() {
		setTimeout(function() {
			if(jQuery(".fancybox-wrap").length > 0) {
				if(jQuery(".fancybox-wrap").hasClass("fancybox-type-iframe")) {
					jQuery(".fancybox-wrap").append("<div class='fullscreen-callout'><div class='arrow-up'></div>Go fullscreen for a better experience</div>");
					setTimeout(function() { jQuery('.fullscreen-callout').fadeOut(500) }, 2900);
				}
			} else {
				appendCallout();
			}
		}, 250);
	}
	jQuery(document).ready(function() {
		jQuery("a.fancybox").on("click", function() {
			appendCallout();
		});
	});
</script>