<?php
/*
Template Name: Simple Template
*/
get_header(); ?>
<div id="main" class="container">
    <section class="contact-section row">
        <?php if(have_posts()) : the_post() ?>
            <div class="descr fivecol">
                    <h1><?php the_title() ?></h1>
                    
            </div>
            <div class="sevencol last"><?php the_content() ?></div>
        <?php endif ?>
    </section>
</div>
<?php get_footer(); ?>