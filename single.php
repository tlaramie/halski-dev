<?php get_header(); ?>
<div id="main" class="container">
	<section class="blog-section">
		<?php if(have_posts()) : the_post() ?>
		<?php $theme_post = get_post_meta(get_the_ID(), '_theme_post', true); ?>
			<article class="post row  <?php echo $theme_post; ?>">
				<?php get_template_part('sidebar', 'block') ?>
				<div class="onecol"></div>
				<div class="descr sevencol">
					<?php $images=&get_children( 'post_parent='.get_the_ID().'&post_type=attachment&post_mime_type=image&orderby=menu_order&order=ASC' );
					if (is_array($images) && !empty($images)) : ?>
						<div class="gallery">	
							<div class="frame" style="position:relative;padding-bottom:56%;">
									<?php 
										$gallerycount = 0;
										foreach($images as $image) : 
											$video_link = $image->post_content;
										?>
										<?php if($video_link) : ?>
											<iframe src="http://player.vimeo.com/video/<?php echo $video_link ?>?title=0&amp;byline=0" style="position:absolute;top:0;bottom:0;left:0;right:0;width:100%;height:100%;" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
										<?php else : ?>
											<?php echo wp_get_attachment_image( $image->ID, '647x469' ) ?>
										<?php endif ?>
									<?php 
										$gallerycount++;
										endforeach;	
									?>
                                    <div class='fullscreen-callout' style='bottom:-15px;right:0;'><div class='arrow-up'></div>Go fullscreen for a better experience</div>
							</div>
							<?php if ($gallerycount > 1) : ?>
							<ul class="switcher">
								<li><a href="#" class="prev">prev</a></li>
								<li><a href="#" class="next">next</a></li>
							</ul>
							<?php endif ?>
						</div>	
					<?php endif ?>
					<div class="text-block">
						<?php the_content() ?>
					</div>
				</div>
				<div class="onecol last"></div>
			</article>
			<div class="comments-block">
				<div class="single-navigation row">
					<div class="next"><?php previous_post_link('%link &rarr;') ?></div>
					<div class="prev"><?php next_post_link('&larr; %link') ?></div>
				</div>
				<div id="comments" class="row">
				<?php comments_template(); ?>
				</div>
			</div>
		<?php else : ?>
			<article class="post">
				<div class="descr">
					<div class="text-block">
						<h1>Not Found</h1>
						<p>Sorry, but you are looking for something that isn't here.</p>
					</div>
				</div>
			</article>
		<?php endif ?>
	</section>
</div>
<?php get_footer(); ?>
<script type="text/JavaScript">
	jQuery(document).ready(function() {
		setTimeout(function() { jQuery('.fullscreen-callout').fadeOut(500) }, 2900);
	});
</script>