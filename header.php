<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="<?php bloginfo('charset'); ?>"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?></title>
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="apple-touch-icon" href="/apple-touch-icon.png">	
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/all.css">
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css">
	<!--<?php
		$page_id = get_queried_object_id();
		if($page_id === 12) { //if about page
			echo '<link type="text/css" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />';
		}
	?>-->
	<!--old typekit script 
    <script type="text/javascript" src="http://use.typekit.com/cqf3ttl.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>-->
    <script src="https://use.typekit.net/apc6slh.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<?php wp_head() ?>
	<script>window.jQuery || document.write('<script src="<?php bloginfo('template_url'); ?>/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
</head>
<body<?php echo is_front_page() ? ' class="home-page"' : '' ?><?php echo 'data-id="'.get_queried_object_id().'"'; ?>
	<div id="wrapper">
		<header id="header" class="container">
			<!-- <div class="holder"> -->
				<div class="frame row">
					<h1 class="twocol logo<?php echo is_front_page() ? ' active' : '' ?>"><a href="<?php bloginfo('url') ?>"><?php bloginfo('name') ?></a></h1>
					<div class="twocol"></div>
					<nav role="navigation" class="eightcol last">
						 <div class="navbar">
						 	<a class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse">menu</a>
						 </div>
						 <div class="nav-collapse collapse">
							<?php wp_nav_menu( array(
								 'container' 		=> 'div',
								 'container_id'		=> 'nav',
								 'theme_location' 	=> 'primary',
								 'depth' 			=> 1,
								 'items_wrap' 		=> '<ul>%3$s</ul>' ) ); ?>
						 </div>
					</nav>
				</div>
			<!-- </div> -->
		</header>